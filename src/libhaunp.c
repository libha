/***********************************************************************
 * This file is part of HA, a general purpose file archiver.
 * Copyright (C) 1995 Harri Hirvola
 * Modified by Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 ***********************************************************************/
#include <setjmp.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "libhaunp.h"


/******************************************************************************/
enum {
  ERR_READ = 1,
  ERR_BAD_DATA
};


/******************************************************************************/
#define POSCODES   (31200)
#define SLCODES    (16)
#define LLCODES    (48)
#define LLLEN      (16)
#define LLBITS     (4)
#define LENCODES   (SLCODES+LLCODES*LLLEN)
#define LTCODES    (SLCODES+LLCODES)
#define CTCODES    (256)
#define PTCODES    (16)
#define LTSTEP     (8)
#define MAXLT      (750*LTSTEP)
#define CTSTEP     (1)
#define MAXCT      (1000*CTSTEP)
#define PTSTEP     (24)
#define MAXPT      (250*PTSTEP)
#define TTSTEP     (40)
#define MAXTT      (150*TTSTEP)
#define TTORD      (4)
#define TTOMASK    (TTORD-1)
#define LCUTOFF    (3*LTSTEP)
#define CCUTOFF    (3*CTSTEP)
#define CPLEN      (8)
#define LPLEN      (4)


/******************************************************************************/
#define RD_BUF_SIZE  (32*1024)
struct haunp_s {
  /* hup */
  uint16_t ltab[2*LTCODES];
  uint16_t eltab[2*LTCODES];
  uint16_t ptab[2*PTCODES];
  uint16_t ctab[2*CTCODES];
  uint16_t ectab[2*CTCODES];
  uint16_t ttab[TTORD][2];
  uint16_t accnt, pmax, npt;
  uint16_t ces;
  uint16_t les;
  uint16_t ttcon;
  /* swd */
  uint8_t dict[POSCODES];
  uint16_t dict_pos;
  uint16_t dict_pair_len;
  uint16_t dict_pair_pos;
  /* ari */
  uint16_t ari_h, ari_l, ari_v;
  int16_t ari_s;
  int16_t ari_gpat, ari_ppat;
  int ari_init_done;
  /* reader */
  haunp_bread_fn_t reader;
  uint8_t rd_buf[RD_BUF_SIZE];
  int rd_size;
  int rd_pos;
  int rd_max;
  int no_more; /* high-level flag: don't call read callback anymore  */
  /* for unpack-from-mem */
  const uint8_t *buf;
  size_t buf_left;
  /* other */
  void *udata;
  /* unpacker */
  int done;
  /* error exit */
  jmp_buf errJP;
};


/******************************************************************************/
/* udata: haunp_t */
static int haunp_buf_reader (void *buf, int buf_len, void *udata) {
  haunp_t hup = (haunp_t)udata;
  if (hup->buf_left > 0) {
    if (buf_len > hup->buf_left) buf_len = hup->buf_left;
    memcpy(buf, hup->buf, buf_len);
    hup->buf += buf_len;
    hup->buf_left -= buf_len;
    return buf_len;
  }
  return 0;
}


/******************************************************************************/
static inline void tabinit (uint16_t t[], uint16_t tl, uint16_t ival) {
  uint16_t i, j;
  for (i = tl; i < 2*tl; ++i) t[i] = ival;
  for (i = tl-1, j = (tl<<1)-2; i; --i, j -= 2) t[i] = t[j]+t[j+1];
}


static inline void tscale (uint16_t t[], uint16_t tl) {
  uint16_t i, j;
  for (i = (tl<<1)-1; i >= tl; --i) if (t[i] > 1) t[i] >>= 1;
  for (i = tl-1, j = (tl<<1)-2; i; --i, j -= 2) t[i] = t[j]+t[j+1];
}


static inline void tupd (uint16_t t[], uint16_t tl, uint16_t maxt, uint16_t step, uint16_t p) {
  int16_t i;
  for (i = p+tl; i; i >>= 1) t[i] += step;
  if (t[1] >= maxt) tscale(t, tl);
}


static inline void tzero (uint16_t t[], uint16_t tl, uint16_t p) {
  int16_t i, step;
  for (i = p+tl, step = t[i]; i; i >>= 1) t[i] -= step;
}


static inline void ttscale (haunp_t hup, uint16_t con) {
  hup->ttab[con][0] >>= 1;
  if (hup->ttab[con][0] == 0) hup->ttab[con][0] = 1;
  hup->ttab[con][1] >>= 1;
  if (hup->ttab[con][1] == 0) hup->ttab[con][1] = 1;
}


/******************************************************************************/
/* return number of bytes copied (can be less thatn olen) */
static inline int swd_do_pair (haunp_t hup, uint8_t *obuf, int olen) {
  int todo = (olen > hup->dict_pair_len ? hup->dict_pair_len : olen), res = todo;
  hup->dict_pair_len -= todo;
  while (todo-- > 0) {
    hup->dict[hup->dict_pos] = hup->dict[hup->dict_pair_pos];
    *obuf++ = hup->dict[hup->dict_pair_pos];
    if (++hup->dict_pos == POSCODES) hup->dict_pos = 0;
    if (++hup->dict_pair_pos == POSCODES) hup->dict_pair_pos = 0;
  }
  return res;
}


/******************************************************************************/
/* Arithmetic decoding                                                        */
/******************************************************************************/
/* read next byte (buffered) */
#define getbyte(bto)  do { \
  if (hup->rd_pos >= hup->rd_max && !hup->no_more) { \
    hup->rd_pos = 0; \
    hup->rd_max = hup->reader(hup->rd_buf, hup->rd_size, hup->udata); \
    hup->no_more = (hup->rd_max <= 0); \
    if (hup->rd_max < 0) longjmp(hup->errJP, ERR_READ); \
  } \
  bto = (!hup->no_more ? hup->rd_buf[hup->rd_pos++] : -1); \
} while (0)


#define getbit(b)  do { \
  hup->ari_gpat <<= 1; \
  if (!(hup->ari_gpat&0xff)) { \
    getbyte(hup->ari_gpat); \
    if (hup->ari_gpat&0x100) { \
      hup->ari_gpat = 0x100; \
    } else { \
      hup->ari_gpat <<= 1; \
      hup->ari_gpat |= 1; \
    } \
  } \
  b |= (hup->ari_gpat&0x100)>>8; \
} while (0)


static void ac_in (haunp_t hup, uint16_t low, uint16_t high, uint16_t tot) {
  uint32_t r;
  if (tot == 0) longjmp(hup->errJP, ERR_BAD_DATA);
  r = (uint32_t)(hup->ari_h-hup->ari_l)+1;
  hup->ari_h = (uint16_t)(r*high/tot-1)+hup->ari_l;
  hup->ari_l += (uint16_t)(r*low/tot);
  while (!((hup->ari_h^hup->ari_l)&0x8000)) {
    hup->ari_l <<= 1;
    hup->ari_h <<= 1;
    hup->ari_h |= 1;
    hup->ari_v <<= 1;
    getbit(hup->ari_v);
  }
  while ((hup->ari_l&0x4000) && !(hup->ari_h&0x4000)) {
    hup->ari_l <<= 1;
    hup->ari_l &= 0x7fff;
    hup->ari_h <<= 1;
    hup->ari_h |= 0x8001;
    hup->ari_v <<= 1;
    hup->ari_v ^= 0x8000;
    getbit(hup->ari_v);
  }
}


static inline uint16_t ac_threshold_val (haunp_t hup, uint16_t tot) {
  uint32_t r = (uint32_t)(hup->ari_h-hup->ari_l)+1;
  if (r == 0) longjmp(hup->errJP, ERR_BAD_DATA);
  return (uint16_t)((((uint32_t)(hup->ari_v-hup->ari_l)+1)*tot-1)/r);
}


/******************************************************************************/
int haunp_reset_io (haunp_t hup, haunp_bread_fn_t reader, void *udata) {
  if (hup != NULL) {
    memset(hup, 0, sizeof(*hup));
    hup->reader = reader;
    hup->udata = udata;
    /* init dictionary */
    hup->dict_pos = 0;
    /* init model */
    hup->ces = CTSTEP;
    hup->les = LTSTEP;
    hup->accnt = 0;
    hup->ttcon = 0;
    hup->npt = hup->pmax = 1;
    for (int i = 0; i < TTORD; ++i) hup->ttab[i][0] = hup->ttab[i][1] = TTSTEP;
    tabinit(hup->ltab, LTCODES, 0);
    tabinit(hup->eltab, LTCODES, 1);
    tabinit(hup->ctab, CTCODES, 0);
    tabinit(hup->ectab, CTCODES, 1);
    tabinit(hup->ptab, PTCODES, 0);
    tupd(hup->ptab, PTCODES, MAXPT, PTSTEP, 0);
    /* init arithmetic decoder */
    hup->ari_h = 0xffff;
    hup->ari_l = 0;
    hup->ari_gpat = 0;
    hup->ari_init_done = 0; /* defer initialization */
    /* read buffer */
    hup->rd_size = RD_BUF_SIZE;
    hup->no_more = 0;
    return 0;
  }
  return -1;
}


int haunp_reset_buf (haunp_t hup, const void *buf, int buf_len) {
  if (hup != NULL) {
    haunp_reset_io(hup, haunp_buf_reader, NULL);
    hup->udata = hup;
    hup->buf = buf;
    hup->buf_left = (buf_len > 0 ? buf_len : 0);
    hup->no_more = (hup->buf_left == 0);
    return 0;
  }
  return -1;
}


haunp_t haunp_open_io (haunp_bread_fn_t reader, void *udata) {
  haunp_t hup = calloc(1, sizeof(*hup));
  if (hup != NULL) haunp_reset_io(hup, reader, udata);
  return hup;
}


haunp_t haunp_open_buf (const void *buf, int buf_len) {
  haunp_t hup = calloc(1, sizeof(*hup));
  if (hup != NULL) haunp_reset_buf(hup, buf, buf_len);
  return hup;
}


int haunp_close (haunp_t hup) {
  if (hup != NULL) free(hup);
  return 0;
}


/******************************************************************************/
static void libha_unpack (haunp_t hup, uint8_t *obuf, int olen) {
  //uint16_t l, p, tv, i, lt;
  hup->done = 0;
  if (hup->no_more) return;
  /* complete defered ari initialization */
  if (!hup->ari_init_done) {
    uint8_t b;
    hup->ari_init_done = 1;
    getbyte(b);
    hup->ari_v = b<<8;
    getbyte(b);
    hup->ari_v |= b;
  }
do_pair:
  /* if we have some data in dictionary, copy it */
  if (hup->dict_pair_len) {
    int d = swd_do_pair(hup, obuf, olen);
    hup->done += d;
    if ((olen -= d) == 0) return;
    obuf += d;
  }
  /* main unpacking loop; olen is definitely positive here */
  do {
    uint16_t l, p, lt;
    uint16_t tv = ac_threshold_val(hup, hup->ttab[hup->ttcon][0]+hup->ttab[hup->ttcon][1]+1);
    uint16_t i = hup->ttab[hup->ttcon][0]+hup->ttab[hup->ttcon][1];
    if (hup->ttab[hup->ttcon][0] > tv) {
      ac_in(hup, 0, hup->ttab[hup->ttcon][0], i+1);
      hup->ttab[hup->ttcon][0] += TTSTEP;
      if (i >= MAXTT) ttscale(hup, hup->ttcon);
      hup->ttcon = (hup->ttcon<<1)&TTOMASK;
      tv = ac_threshold_val(hup, hup->ctab[1]+hup->ces);
      if (tv >= hup->ctab[1]) {
        ac_in(hup, hup->ctab[1], hup->ctab[1]+hup->ces, hup->ctab[1]+hup->ces);
        tv = ac_threshold_val(hup, hup->ectab[1]);
        l = 2;
        lt = 0;
        for (;;) {
          if (lt+hup->ectab[l] <= tv) { lt += hup->ectab[l]; ++l; }
          if (l >= CTCODES) { l -= CTCODES; break; }
          l <<= 1;
        }
        ac_in(hup, lt, lt+hup->ectab[CTCODES+l], hup->ectab[1]);
        tzero(hup->ectab, CTCODES, l);
        if (hup->ectab[1] != 0) hup->ces += CTSTEP; else hup->ces = 0;
        for (i = (l < CPLEN ? 0 : l-CPLEN); i < (l+CPLEN >= CTCODES-1 ? CTCODES-1 : l+CPLEN); ++i) {
          if (hup->ectab[CTCODES+i]) tupd(hup->ectab, CTCODES, MAXCT, 1, i);
        }
      } else {
        l = 2;
        lt = 0;
        for (;;) {
          if (lt+hup->ctab[l] <= tv) { lt += hup->ctab[l]; ++l; }
          if (l >= CTCODES) { l -= CTCODES; break; }
          l <<= 1;
        }
        ac_in(hup, lt, lt+hup->ctab[CTCODES+l], hup->ctab[1]+hup->ces);
      }
      tupd(hup->ctab, CTCODES, MAXCT, CTSTEP, l);
      if (hup->ctab[CTCODES+l] == CCUTOFF) hup->ces -= (CTSTEP < hup->ces ? CTSTEP : hup->ces-1);
      /* literal char from dictionary */
      hup->dict[hup->dict_pos] = l;
      if (++hup->dict_pos == POSCODES) hup->dict_pos = 0;
      /* asc decoder */
      if (hup->accnt < POSCODES) ++hup->accnt;
      /* output char */
      *obuf++ = l;
      --olen;
      ++hup->done;
    } else if (i > tv) {
      ac_in(hup, hup->ttab[hup->ttcon][0], i, i+1);
      hup->ttab[hup->ttcon][1] += TTSTEP;
      if (i >= MAXTT) ttscale(hup, hup->ttcon);
      hup->ttcon = ((hup->ttcon<<1)|1)&TTOMASK;
      while (hup->accnt > hup->pmax) {
        tupd(hup->ptab, PTCODES, MAXPT, PTSTEP, hup->npt++);
        hup->pmax <<= 1;
      }
      tv = ac_threshold_val(hup, hup->ptab[1]);
      p = 2;
      lt = 0;
      for (;;) {
        if (lt+hup->ptab[p] <= tv) { lt += hup->ptab[p]; ++p; }
        if (p >= PTCODES) { p -= PTCODES; break; }
        p <<= 1;
      }
      ac_in(hup, lt, lt+hup->ptab[PTCODES+p], hup->ptab[1]);
      tupd(hup->ptab, PTCODES, MAXPT, PTSTEP, p);
      if (p > 1) {
        for (i = 1; p; i <<= 1, --p) ;
        i >>= 1;
        l = (i == hup->pmax>>1 ? hup->accnt-(hup->pmax>>1) : i);
        p = ac_threshold_val(hup, l);
        ac_in(hup, p, p+1, l);
        p += i;
      }
      tv = ac_threshold_val(hup, hup->ltab[1]+hup->les);
      if (tv >= hup->ltab[1]) {
        ac_in(hup, hup->ltab[1], hup->ltab[1]+hup->les, hup->ltab[1]+hup->les);
        tv = ac_threshold_val(hup, hup->eltab[1]);
        l = 2;
        lt = 0;
        for (;;) {
          if (lt+hup->eltab[l] <= tv) { lt += hup->eltab[l]; ++l; }
          if (l >= LTCODES) { l -= LTCODES; break; }
          l <<= 1;
        }
        ac_in(hup, lt, lt+hup->eltab[LTCODES+l], hup->eltab[1]);
        tzero(hup->eltab, LTCODES, l);
        if (hup->eltab[1] != 0) hup->les += LTSTEP; else hup->les = 0;
        for (i = l < LPLEN ? 0 : l-LPLEN; i < (l+LPLEN >= LTCODES-1 ? LTCODES-1 : l+LPLEN); ++i) {
          if (hup->eltab[LTCODES+i]) tupd(hup->eltab, LTCODES, MAXLT, 1, i);
        }
      } else {
        l = 2;
        lt = 0;
        for (;;) {
          if (lt+hup->ltab[l] <= tv) { lt += hup->ltab[l]; ++l; }
          if (l >= LTCODES) { l -= LTCODES; break; }
          l <<= 1;
        }
        ac_in(hup, lt, lt+hup->ltab[LTCODES+l], hup->ltab[1]+hup->les);
      }
      tupd(hup->ltab, LTCODES, MAXLT, LTSTEP, l);
      if (hup->ltab[LTCODES+l] == LCUTOFF) hup->les -= (LTSTEP < hup->les ? LTSTEP : hup->les-1);
      if (l == SLCODES-1) {
        l = LENCODES-1;
      } else if (l >= SLCODES) {
        i = ac_threshold_val(hup, LLLEN);
        ac_in(hup, i, i+1, LLLEN);
        l = ((l-SLCODES)<<LLBITS)+i+SLCODES-1;
      }
      l += 3;
      if (hup->accnt < POSCODES) {
        hup->accnt += l;
        if (hup->accnt > POSCODES) hup->accnt = POSCODES;
      }
      /* pair from dictionary */
      if (hup->dict_pos > p) {
        hup->dict_pair_pos = hup->dict_pos-1-p;
      } else {
        hup->dict_pair_pos = POSCODES-1-p+hup->dict_pos;
      }
      hup->dict_pair_len = l;
      goto do_pair; /* recursive tail call */
    } else {
      /*EOF*/
      /*ac_in(hup, i, i+1, i+1); don't need this*/
      hup->no_more = 1;
      break;
    }
  } while (olen > 0);
}


/* return number of bytes read (<len: end of data) or -1 on error */
int haunp_read (haunp_t hup, void *buf, int len) {
  if (buf == NULL && len < 1) return 0;
  if (hup != NULL) {
    volatile int jr = setjmp(hup->errJP);
    if (jr == 0) {
      hup->done = 0;
      libha_unpack(hup, buf, len);
    }
    return hup->done;
  }
  return -1;
}


/******************************************************************************/
#ifndef LIBHAUNP_DISABLE_CRC
static const uint32_t crctab[16] = {
  0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
  0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
  0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
  0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c,
};


unsigned int haunp_crc32 (const void *src, int len) {
  uint32_t crc = 0xffffffffUL;
  if (src != NULL && len > 0) {
    const uint8_t *buf = (const uint8_t *)src;
    while (len--) {
      crc ^= *buf++;
      crc = crctab[crc&0x0f]^(crc>>4);
      crc = crctab[crc&0x0f]^(crc>>4);
    }
  }
  return crc^0xffffffffUL;
}


unsigned int haunp_crc32_begin (void) {
  return 0xffffffffUL;
}


unsigned int haunp_crc32_end (unsigned int crc) {
  return crc^0xffffffffUL;
}


unsigned int haunp_crc32_part (unsigned int crc, const void *src, int len) {
  if (src != NULL && len > 0) {
    const uint8_t *buf = (const uint8_t *)src;
    while (len--) {
      crc ^= *buf++;
      crc = crctab[crc&0x0f]^(crc>>4);
      crc = crctab[crc&0x0f]^(crc>>4);
    }
  }
  return crc;
}
#endif
