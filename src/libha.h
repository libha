/***********************************************************************
 * This file is part of HA, a general purpose file archiver.
 * Copyright (C) 1995 Harri Hirvola
 * Modified by Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 ***********************************************************************/
#ifndef LIBHA_H
#define LIBHA_H

/* define this to disable unpacker */
/*#define LIBHA_DISABLE_UNPACKER*/

/* define this to disable CRC module */
/*#define LIBHA_DISABLE_CRC*/

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************/
enum {
  LIBHA_ERR_OK = 0,
  LIBHA_ERR_READ = -1,
  LIBHA_ERR_WRITE = -2,
  LIBHA_ERR_BAD_DATA = -3
};


/******************************************************************************/
typedef struct libha_io_s {
  int (*bread) (void *buf, int buf_len, void *udata); /* return number of bytes read; 0: EOF; <0: error; can be less than buf_len */
  int (*bwrite) (const void *buf, int buf_len, void *udata); /* result != buf_len: error */
} libha_io_t;


/******************************************************************************/
typedef struct libha_s *libha_t;


extern libha_t libha_alloc (const libha_io_t *iot, void *udata);
extern void libha_free (libha_t asc);

extern const libha_io_t *libha_get_iot (libha_t asc);
extern int libha_set_iot (libha_t asc, const libha_io_t *iot); /* <0: error; 0: ok */

extern void *libha_get_udata (libha_t asc);
extern int libha_set_udata (libha_t asc, void *udata); /* <0: error; 0: ok */

/* you need to set i/o structure before calling pack or unpack */
extern int libha_pack (libha_t asc); /* LIBHA_ERR_xxx */
#ifndef LIBHA_DISABLE_UNPACKER
extern int libha_unpack (libha_t asc); /* LIBHA_ERR_xxx */
#endif


/******************************************************************************/
/* poly: 0xedb88320L: ISO 3309, ITU-T V.42 */
/* unsigned int should be at least 32 bit */
#ifndef LIBHA_DISABLE_CRC
extern unsigned int libha_crc32 (const void *src, int len);
extern unsigned int libha_crc32_begin (void);
extern unsigned int libha_crc32_end (unsigned int crc);
extern unsigned int libha_crc32_part (unsigned int crc, const void *src, int len);
#endif


#ifdef __cplusplus
}
#endif
#endif
