/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _PBAR_H_
#define _PBAR_H_

#include <stdint.h>


extern void pbar_draw (uint64_t cur, uint64_t total, uint64_t taken);
extern void pbar_clear (void);
extern void pbar_newline (void);

/* get dot count; can be used to avoid excess redraws */
extern int pbar_dot_count (uint64_t cur, uint64_t total, uint64_t taken);

/* dest: at least 92 bytes */
extern void pbar_u2s (char *dest, uint64_t i);


#endif
