/***********************************************************************
 * This file is part of HA, a general purpose file archiver.
 * Copyright (C) 1995 Harri Hirvola
 * Modified by Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 ***********************************************************************/
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "libha.h"
#include "pbar.h"

#include "intio.c"


static libha_t asc = NULL;
static int fdi = -1;
static int fdo = -1;

static int dot_count = -1;

static uint64_t bytes_done = 0, bytes_total = 0, bytes_res = 0;
static int packing;

static unsigned int crc_cur, crc;


static int bread (void *buf, int buf_len, void *udata) {
  static int was_eof = 0;
  int res;
  if (packing) {
    if (pbar_dot_count(bytes_done, bytes_total, bytes_res) != dot_count) {
      dot_count = pbar_dot_count(bytes_done, bytes_total, bytes_res);
      pbar_draw(bytes_done, bytes_total, bytes_res);
    }
  }
  res = read(fdi, buf, buf_len);
  if (res > 0) {
    if (packing) {
      bytes_done += res;
      crc_cur = libha_crc32_part(crc_cur, buf, res);
    } else {
      bytes_res += res;
    }
  } else {
    if (was_eof) { fprintf(stderr, "\n***TTTSNB:0\n"); abort(); } /* the thing that should not be */
    was_eof = 1;
  }
  if (was_eof && packing && bytes_done > bytes_total) { fprintf(stderr, "\n***TTTSNB:1\n"); abort(); } /* the thing that should not be */
  return res;
}


static int bwrite (const void *buf, int buf_len, void *udata) {
  static int was_eof = 0;
  int res = write(fdo, buf, buf_len);
  if (res > 0) {
    if (!packing) {
      bytes_done += res;
      crc_cur = libha_crc32_part(crc_cur, buf, res);
    } else {
      bytes_res += res;
    }
  } else {
    if (was_eof) { fprintf(stderr, "\n***TTTSNB:2\n"); abort(); } /* the thing that should not be */
    was_eof = 1;
  }
  if (!packing) {
    if (pbar_dot_count(bytes_done, bytes_total, bytes_res) != dot_count) {
      dot_count = pbar_dot_count(bytes_done, bytes_total, bytes_res);
      pbar_draw(bytes_done, bytes_total, bytes_res);
    }
    if (was_eof && bytes_done > bytes_total) { fprintf(stderr, "\n***TTTSNB:3\n"); abort(); } /* the thing that should not be */
  }
  return res;
}


static const libha_io_t haio = {
  .bread = bread,
  .bwrite = bwrite,
};


int main (int argc, char *argv[]) {
  int res;
  if (argc != 4) {
    fprintf(stderr, "usage: %s <e|d> infile outfile\n", argv[0]);
    return 1;
  }
  if (strcmp(argv[1], "e") == 0 || strcmp(argv[1], "c") == 0) packing = 1;
  else if (strcmp(argv[1], "d") == 0 || strcmp(argv[1], "x") == 0) packing = 0;
  else {
    fprintf(stderr, "FATAL: unknown mode: '%s'\n", argv[1]);
    return 1;
  }
  fdi = open(argv[2], O_RDONLY|O_CLOEXEC);
  if (fdi < 0) {
    fprintf(stderr, "FATAL: can't open file: '%s'\n", argv[2]);
    return 1;
  }
  fdo = open(argv[3], O_WRONLY|O_CREAT|O_TRUNC|O_CLOEXEC, 0640);
  if (fdo < 0) {
    fprintf(stderr, "FATAL: can't create file: '%s'\n", argv[3]);
    return 1;
  }
  bytes_total = lseek(fdi, 0, SEEK_END);
  lseek(fdi, 0, SEEK_SET);
  asc = libha_alloc(&haio, NULL);
  crc_cur = libha_crc32_begin();
  if (packing) {
    /* hey, write signature */
    if (write(fdo, "LBHZ", 4) != 4) res = -1;
    else if (write(fdo, &crc_cur, 4) != 4) res = -1;
    else if (iio_write_integer_fd(fdo, &bytes_total, 8) < 0) res = -1;
    else res = libha_pack(asc);
    /* write CRC */
    if (res == 0) {
      crc_cur = libha_crc32_end(crc_cur);
      lseek(fdo, 4, SEEK_SET);
      if (write(fdo, &crc_cur, 4) != 4) res = -1;
    }
  } else {
    char sign[4];
    if (read(fdi, sign, 4) != 4) res = -1;
    else if (memcmp(sign, "LBHZ", 4) != 0) res = -1;
    else if (read(fdi, &crc, 4) != 4) res = -1;
    else if (iio_read_integer_fd(fdi, &bytes_total, 8) < 0) res = -1;
    else res = libha_unpack(asc);
    if (res == 0 && bytes_done != bytes_total) res = -999;
    if (res == 0) {
      crc_cur = libha_crc32_end(crc_cur);
      if (crc != crc_cur) res = -666; /* CRC error */
    }
  }
  libha_free(asc);
  pbar_clear();
  close(fdi);
  close(fdo);
  switch (res) {
    case 0: break;
    case LIBHA_ERR_READ: fprintf(stderr, "\nREADING ERROR!\n"); break;
    case LIBHA_ERR_WRITE: fprintf(stderr, "\nWRITING ERROR!\n"); break;
    case LIBHA_ERR_BAD_DATA: fprintf(stderr, "\nBAD DATA!\n"); break;
    case -666: fprintf(stderr, "\nCRC ERROR!\n"); break;
    case -999: fprintf(stderr, "\nSIZE ERROR!\n"); break;
  }
  if (res != 0) {
    unlink(argv[3]);
  } else {
    char stot[92], sres[92];
    pbar_u2s(stot, bytes_total);
    pbar_u2s(sres, bytes_res);
    if (packing) {
      printf("%s bytes packed to %s bytes; compress ratio: %.1f%%\n", stot, sres, 100.0-(100.0*(double)bytes_res/(double)bytes_total));
    } else {
      printf("%s bytes unpacked to %s bytes\n", sres, stot);
    }
  }
  return res;
}
