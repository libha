/***********************************************************************
 * This file is part of HA, a general purpose file archiver.
 * Copyright (C) 1995 Harri Hirvola
 * Modified by Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 ***********************************************************************/
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "libhaunp.h"
#include "pbar.h"

#include "intio.c"


static int fdi = -1;
static int fdo = -1;
static uint64_t bytes_done = 0, bytes_total = 0, bytes_res = 0, left;

static unsigned int crc_cur, crc;

#ifdef NDEBUG
# define OUTBUF_SIZE  (1024*1024)
#else
# define OUTBUF_SIZE  1
#endif
static uint8_t *wrbuf;


static int dot_count = -1;


static int bread (void *buf, int buf_len, void *udata) {
  int res = read(fdi, buf, buf_len);
  if (res > 0) bytes_res += res;
  return res;
}


int main (int argc, char *argv[]) {
  haunp_t hup;
  int res = 0;
  char sign[4];
#ifndef NDEBUG
  if (argc != 3) {
    argc = 3;
    argv[1] = "egatiles.dd2.haz";
    argv[2] = "z01";
  }
#endif
  if (argc != 3) {
    fprintf(stderr, "usage: %s infile outfile\n", argv[0]);
    return 1;
  }
  fdi = open(argv[1], O_RDONLY|O_CLOEXEC);
  if (fdi < 0) {
    fprintf(stderr, "FATAL: can't open file: '%s'\n", argv[1]);
    return 1;
  }
  if (read(fdi, sign, 4) != 4) res = -1;
  else if (memcmp(sign, "LBHZ", 4) != 0) res = -1;
  else if (read(fdi, &crc, 4) != 4) res = -1;
  else if (iio_read_integer_fd(fdi, &bytes_total, 8) < 0) res = -1;
  if (res == -1) {
    close(fdi);
    fprintf(stderr, "FATAL: not libha file!\n");
    return 1;
  }
  fdo = open(argv[2], O_WRONLY|O_CREAT|O_TRUNC|O_CLOEXEC, 0640);
  if (fdo < 0) {
    fprintf(stderr, "FATAL: can't create file: '%s'\n", argv[2]);
    return 1;
  }
  hup = haunp_open_io(bread, NULL);
  wrbuf = malloc(OUTBUF_SIZE);
  //printf("output buffer size: %d\n", OUTBUF_SIZE);
  crc_cur = haunp_crc32_begin();
  left = bytes_total;
  while (left > 0) {
    int rd = (left > OUTBUF_SIZE ? OUTBUF_SIZE : left);
    if (pbar_dot_count(bytes_done, bytes_total, bytes_res) != dot_count) {
      dot_count = pbar_dot_count(bytes_done, bytes_total, bytes_res);
      pbar_draw(bytes_done, bytes_total, bytes_res);
    }
    rd = haunp_read(hup, wrbuf, rd);
    if (rd <= 0) {
      pbar_clear();
      if (rd < 0 || left > 0) {
        fprintf(stdout, "READ ERROR!\n");
        res = -1;
      }
      break;
    }
    crc_cur = haunp_crc32_part(crc_cur, wrbuf, rd);
    if (write(fdo, wrbuf, rd) != rd) {
      pbar_clear();
      fprintf(stdout, "WRITE ERROR!\n");
      res = -1;
      break;
    }
    bytes_done += rd;
    left -= rd;
  }
  free(wrbuf);
  pbar_clear();
  haunp_close(hup);
  close(fdi);
  close(fdo);
  if (res == 0) {
    crc_cur = haunp_crc32_end(crc_cur);
    if (crc_cur != crc) {
      fprintf(stdout, "CRC ERROR!\n");
      res = -1;
    }
  }
  if (res != 0) {
    unlink(argv[2]);
  } else {
    char stot[92], sres[92];
    pbar_u2s(stot, bytes_total);
    pbar_u2s(sres, bytes_res);
    printf("%s bytes unpacked to %s bytes\n", sres, stot);
  }
  return res;
}
