#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/ioctl.h>

#include "pbar.h"


static int pbar_tty; // fd or -1


static __attribute__((constructor)) void _ctor_pbar (void) {
  if (isatty(STDOUT_FILENO)) pbar_tty = STDOUT_FILENO;
  else if (isatty(STDERR_FILENO)) pbar_tty = STDERR_FILENO;
  else pbar_tty = -1;
}


static int get_tty_width (void) {
  if (pbar_tty >= 0) {
    struct winsize ws;
    if (ioctl(pbar_tty, TIOCGWINSZ, &ws) != 0 || ws.ws_col < 2) return 79;
    return ws.ws_col-1;
  }
  return 79;
}


void pbar_u2s (char *dest, uint64_t i) {
  char num[128], *p = num+sizeof(num);
  int cnt = 3;
  *(--p) = 0;
  for (;;) {
    *(--p) = (i%10)+'0';
    if ((i /= 10) == 0) break;
    if (--cnt == 0) { *(--p) = ','; cnt = 3; }
  }
  strcpy(dest, p);
}


/* return number of dots */
static int pbar_draw_ex (uint64_t cur, uint64_t total, uint64_t taken, int silent) {
  if (pbar_tty >= 0) {
    char cstr[128], tstr[128], wstr[1025];
    int slen, wdt;
    if (cur > total) cur = total;
    if (taken > total) taken = total;
    pbar_u2s(cstr, cur);
    pbar_u2s(tstr, total);
    wdt = get_tty_width();
    if (wdt > 1024) wdt = 1024;
    /* numbers */
    sprintf(wstr, "\r[%*s/%s]", strlen(tstr), cstr, tstr);
    slen = strlen(wstr);
    if (!silent) write(pbar_tty, wstr, slen);
    slen += 7;
    if (slen+1 <= wdt) {
      /* dots */
      int pos;
      int dc = (cur >= total ? wdt-slen : ((uint64_t)(wdt-slen))*cur/total);
      int tc = (taken >= total ? wdt-slen : ((uint64_t)(wdt-slen))*taken/total);
      strcpy(wstr, " [");
      pos = strlen(wstr);
      if (cur == 0) dc = -1;
      if (silent) return ((tc+1)<<16)|(dc+1); /* just return number of dots */
      for (int f = 0; f < wdt-slen; ++f) {
        if (f <= dc) {
          wstr[pos++] = (f <= tc ? ':' : '.');
        } else {
          wstr[pos++] = (f <= tc ? '*' : ' ');
        }
      }
      wstr[pos++] = ']';
      write(pbar_tty, wstr, pos);
      sprintf(cstr, "%4d%%", (int)(total > 0 ? (uint64_t)100*cur/total : 100));
      write(pbar_tty, cstr, strlen(cstr));
    }
  }
  return 0;
}


void pbar_draw (uint64_t cur, uint64_t total, uint64_t taken) { pbar_draw_ex(cur, total, taken, 0); }
int pbar_dot_count (uint64_t cur, uint64_t total, uint64_t taken) { return pbar_draw_ex(cur, total, taken, 1); }


void pbar_clear (void) {
  static const char *cstr = "\r\x1b[K";
  if (pbar_tty >= 0) write(pbar_tty, cstr, strlen(cstr));
}


void pbar_newline (void) {
  if (pbar_tty >= 0) write(pbar_tty, "\r\n", 2); /* "\r\n" for strange tty modes */
}
