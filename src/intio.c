#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
# define IIO_ENDIANNESS_INTEL
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
# define IIO_ENDIANNESS_MOTOROLA
#else
# error Is this really PDP? C'mon, don't shit me!
#endif


/* i/o callbacks */
typedef int (*iio_getbyte_fn_t) (void *udata); /* return [0..255] or -1 on EOF/error */
typedef int (*iio_putbyte_fn_t) (unsigned char bt, void *udata); /* return 0 on OK or -1 on error */


/* integer format is:
 * first byte:
 *   bit 7: set if the number is negative
 *   bit 6: set if we have another byte of data
 *   bits 0-5: data
 * next bytes:
 *   bit 7: set if we have another byte of data
 *   bits 0-6: data
 * bits processed from lsb to msb
 * special case: 0x80 means number 0x80*
 */


/* write an integer value; return # of written bytes on ok OK or -1 on error */
static __attribute__((unused)) int iio_write_integer (const void *src, int size, iio_putbyte_fn_t putb, void *udata) {
  if (src != NULL && size > 0 && size <= 8) {
    unsigned char c;
    union {
      uint64_t i;
      uint8_t b[8];
    } num;
    const unsigned char *sp = (const unsigned char *)src;
    int bcnt = 1;
    /* now move bytes to appropriate place */
#ifdef IIO_ENDIANNESS_INTEL
    num.i = (sp[0]&0x80 ? __UINT64_C(0xffffffffffffffff) : 0); /* clear */
    for (int f = 0; f < size; ++f) num.b[f] = sp[f];
#else
    num.i = (sp[size-1]&0x80 ? __UINT64_C(0xffffffffffffffff) : 0); /* clear */
    for (int f = 0; f < size; ++f) num.b[8-size+f] = sp[f];
#endif
    /* check for special case: 0x80* */
    if (num.i == __UINT64_C(0x8000000000000000)) return (putb == NULL || putb(0x80, udata) == 0 ? 1 : -1); /* yeah, this is the special case */
    /* negative? negate */
    if (num.i&__UINT64_C(0x8000000000000000)) {
      c = 0x80;
      /* this is negation in two's complement arithmetic; it can't overflow, 'cause special case is already processed */
      num.i ^= __UINT64_C(0xffffffffffffffff);
      ++num.i;
    } else {
      c = 0;
    }
    /* store 6 bits of data */
    c |= num.i&0x3f;
    num.i >>= 6;
    if (num.i) c |= 0x40; /* have more data? */
    if (putb != NULL && putb(c, udata) != 0) return -1;
    /* write other bytes */
    while (num.i) {
      /* store 7 bits of data */
      c = num.i&0x7f;
      num.i >>= 7;
      if (num.i) c |= 0x80; /* have more data? */
      if (putb != NULL && putb(c, udata) != 0) return -1;
      ++bcnt;
    }
    return bcnt;
  }
  return -1;
}


/* read an integer value; return # of bytes read or -1 on error */
static __attribute__((unused)) int iio_read_integer (void *dest, int size, iio_getbyte_fn_t getb, void *udata) {
  if (dest != NULL && size > 0 && size <= 8 && getb != NULL) {
    int c;
    union {
      uint64_t i;
      uint8_t b[8];
    } num;
    unsigned char *dp = (unsigned char *)dest;
    int bcnt = 1;
    if ((c = getb(udata)) < 0) return -1;
    /* special case? */
    if (c == 0x80) {
      num.i = __UINT64_C(0x8000000000000000);
    } else {
      int neg = (c&0x80);
      num.i = c&0x3f;
      /* have more data? */
      if (c&0x40) {
        int shift = 6;
        /*TODO: stricter check using 'shift'*/
        do {
          if ((c = getb(udata)) < 0) return -1;
          /* max bytes in number: 64/7+1 */
          if (++bcnt > 10) return -1; /* too many bytes */
          num.i |= ((uint64_t)(c&0x7f))<<shift;
          shift += 7;
        } while (c&0x80);
      }
      if (neg) {
        /* this is negation in two's complement arithmetic; it can't overflow, 'cause special case is already processed */
        num.i ^= __UINT64_C(0xffffffffffffffff);
        ++num.i;
      }
    }
    /* now move bytes to appropriate place */
#ifdef IIO_ENDIANNESS_INTEL
    for (int f = 0; f < size; ++f) dp[f] = num.b[f];
#else
    for (int f = 0; f < size; ++f) dp[f] = num.b[8-size+f];
#endif
    return bcnt;
  }
  return -1;
}


#ifdef IIO_ENDIANNESS_INTEL
# undef IIO_ENDIANNESS_INTEL
#endif
#ifdef IIO_ENDIANNESS_MOTOROLA
# undef IIO_ENDIANNESS_MOTOROLA
#endif


/* handy helpers */
static __attribute__((unused)) int iio_getb_fd (void *udata) {
  unsigned char bt;
  if (read((long)udata, &bt, 1) != 1) return -1;
  return bt;
}


static __attribute__((unused)) int iio_putb_fd (unsigned char bt, void *udata) {
  if (write((long)udata, &bt, 1) != 1) return -1;
  return 0;
}


static __attribute__((unused)) int iio_write_integer_fd (int fd, const void *src, int size) { return iio_write_integer(src, size, iio_putb_fd, (void *)fd); }
static __attribute__((unused)) int iio_read_integer_fd (int fd, void *dest, int size) { return iio_read_integer(dest, size, iio_getb_fd, (void *)fd); }
